<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<meta name="author" content="Flux User">
		<meta name="description" content="My Website">
		<meta name="keywords" content="Flux, Mac">
		<title>Enviormental Change List</title>
			<link rel="stylesheet" type="text/css" href="EnvList.css">
			<script src="Dependencies/jquery.js"></script>
			<script>
	        function Back(){
	            document.getElementById('Back').className ='BackTrans';
				document.getElementById('Add').className ='AddTrans';
				document.getElementById('Remove').className ='RemoveTrans';
				var list = $(".List").map(function() {
					this.className ='ListTrans';
				});
			
				setTimeout(function() { window.location = "../index.html"}, 1000);
	        }
			</script>
		</head>
		<body>
			<div id="Back" class="Back" onclick="Back()">Back</div>
	<?php
	$JSON = file_get_contents("./Information/EnvChanges.json"); // Get the list of mutations
	$EC = json_decode($JSON, true); // Decode into an array
	$i = 0; // Get an array counter

	foreach($EC as $val) {
		$counter = $i + 1; // Set a counter for the page
		$place = '<div id="List" class="List">' . $counter . ". " . $EC[$i][0]["Name"] . ' [<span class="neg">' . implode(', ', $EC[$i][1]["Negate"]) . '</span>] (<span class="add">' . implode(', ', $EC[$i][1]["Add"]) . "</span>)</div>"; // Set a div; output the page counter; output a seperator (.); output the mutation name; output HTML span; output the requirements; output HTML span close and new; output the replacements; output a span and div close
	
		$place = str_replace('[<span class="neg"></span>]', "", $place); // If there's no requirements, don't show them
		echo str_replace('(<span class="add"></span>)', "", $place); // If there's no replacements, don't show them
	
		echo '<div id="Desc" class="Desc">' . $EC[$i][0]["Description"] . '</div>'; // Output the description
	
		$i++; // Increment the array counter
	}?>
	<br>
	<br>
	<div id="Add" class="Add">
		<?
		$failed = null;
		
		if(isset($_GET["failed"])) {
			$failed = $_GET["failed"];
		}
		if($failed) {
			?> <div><font color="red" face="arial"> Incorrect Password </font> </div> <?php
		}
		?>
	
		<div><font color="red" face="arial" size="50"> BELOW: WORK IN PROGRESS </font> </div>
		Add an Envoirmental Change:
	<form action="MutationAdd.php" method="post">
	Mutation: <input type="text" name="mut"><br>
	Requirements: <input type="text" name="req"><br>
	Replaces: <input type="text" name="rep"><br>
	Password: <input type="password" name="addpass"><br>
	<input type="submit">
	</form>
	</div>
	<br>
	<br>
	<div id="Remove" class="Remove">
		<form action="MutationRemove.php" method="post">
			Remove an Enviormental Change:<br>
			Number: <input type="number" name="rNum"><br>
			Password: <input type="password" name="rempass"><br>
			<input type="submit">
		</form>
	</div>

		</body>
</html>