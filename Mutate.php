<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<meta name="author" content="Flux User">
		<meta name="description" content="Hundreds of years pass...">
		<meta name="keywords" content="Flux, Mac">

		<title>Mutate</title>

		<title>My Webpage</title>

		<link rel="stylesheet" type="text/css" href="MutationList.css">
		<link rel="stylesheet" type="text/css" href="Mutate.css">
		<script src="Dependencies/jquery.js"></script>
		<script>
        function Change(){
            document.getElementById('Select').className ='SelectTrans';
			document.getElementById('Next').className ='NextYes';
			document.getElementById('Back').className ='BackNo';
			var list = $(".List").map(function() {
				this.className ='ListTrans';
			});
			
			setTimeout(function() { window.location = "../EnvChange.php"}, 1000);
        }
		
        function Back(){
            document.getElementById('Select').className ='SelectTrans';
			document.getElementById('Next').className ='NextNo';
			document.getElementById('Back').className ='BackYes';
			var list = $(".List").map(function() {
				this.className ='ListTrans';
			});
			
			setTimeout(function() { window.location = "../index.html"}, 1000);
        }
		</script>
	</head>
	<body>
		<div id="Select" class="Select">Select a Mutation:</div>
		<?php
		header("Cache-control: no-cache");
		header("Pragma: no-cache");

		// GET DER ERRRURS
		//ini_set('display_errors', 1);
		//error_reporting(E_ALL ^ E_NOTICE);

		

		// Get list of mutations
		$JSON = file_get_contents("Information/Mutations.json");
		$Mut = json_decode($JSON, true);
		
		// Get settings
		$setFile = file_get_contents("Information/Settings.json");
		$settings = json_decode($setFile, true);
		
		// Init a storage array for the selected mutation names, descriptions, and numbers
		$selectedMutations = array();
		
		// Start the loop
		for($inc = 0; $inc < $settings["MutPerRound"];) { // Init an incrementor, and keep going as long as $inc is less than the number of mutations per round as defined in settings.
			$top = count($Mut) - 1; // Get the max array number
			$rand = rand(0, $top); // Get a random number
			if(! in_array($rand, $selectedMutations)) { // If the random number has already been selected, try again
				$selectedMutations[$inc] = $rand; // Store the number
				$inc++; // Increase the increment
			} // End if statement
		} // End for loop
		
		// Sort numerically
		sort($selectedMutations, SORT_NATURAL);
		//
		for($arInc = 0; $arInc < $settings["MutPerRound"];) {
			$id = $selectedMutations[$arInc];
			$mutNumb = $id + 1;
			$requires = array();
			$replaces = array();
			
			foreach($Mut[$id][1]["Requires"] as $value) {
				$requires[] = $value;
			}
			
			foreach($Mut[$id][1]["Replaces"] as $value) {
				$replaces[] = $value;
			}
			
			echo '<div id="List" class="List">', $mutNumb . ". ", $Mut[$id][0]["Name"], ( empty($requires) ? "" : ' [<span class="req">' . implode(', ', $requires) . '</span>]' ), (empty($replaces) ? "" : ' (<span class="rep">' . implode(', ', $replaces) . '</span>)</div>' );
			
			// Okay, woah. Sorry about that monstrosity. HTML div, ID number, Mutation name, if there's no requirements don't show the brackets, HTML, list the requirements, HTML, if there's no replacements don't show the paranthesees, HTML, list the replacements, HTML
			
			echo '<div id="Desc" class="Desc">' . $Mut[$id][0]["Description"] . '</div>'; // Output the description
			
			$arInc++; // Increment
	}
		?>
		<div id="Next" class="Next" onclick="Change()"></div>
		<div id="Back" class="Back" onclick="Back()">Back</div>
	</body>
</html>